
public class Tachenrechner 

	public static void main(String[] args) {

		  //Deklaration (Dem Programm wird gesagt, welcher Datentyp die Variable hat)
		  public double ergebnis;                                    //public bedeudet, dass alles (jede Klasse...) auf das Programm zugreifen darf. Das public ist nur beispielhaft eigentlich solle hier private stehen
		  private double zahl1;                                      //private bedeudet, dass diese Variable nur von dieser Klasse aufgerufen werden kann
		  private double zahl2;
		  private char operator;

		  public void rechnen() {                                    //muss public hin, da der Erzeuger darauf zugreift. (Methode wird erstellt
		    zahl1 = gibZahl();                                       //die Variable zahl1 ist gleich das, was die Methode gibZahl() zur�ckgibt
		    zahl2 = gibZahl();                                       //die Variable zahl2 ist gleich das, was die Methode gibZahl() zur�ckgibt
		    operator = gibOperator();                                //die Variable operator ist gleich dem, dass die Methode gibOperator() zur�ckgibt
		    
		    //Beginn der eigentlichen Rechnung
		    if (operator == '+') {                                   //eine if Abfrage ob in der Variable operator ein plus ist
		      ergebnis = addition(zahl1, zahl2);                     //die Variable ist ergebnis ist gleich dem, was die Methode addition(zahl1, zahl2) zur�ckgibt. Die Klammern zeigen welcche Variablen verwendet werden
		      System.out.println("Ergebnis "+ergebnis);
		      } else {
		      if (operator == '-') {
		        ergebnis = subtraktion(zahl1, zahl2);

		        } else {
		        if (operator == '*') {
		          ergebnis = mulitplikation(zahl1, zahl2);

		          } else {
		          if (operator == '/') {
		            ergebnis = division(zahl1, zahl2);
		            }
		          }
		        }
		      }
		    }                                                        //Ende der Methode rechnen

		  //Hier die Methoden
		  public double gibZahl() {
		    return Tastatur.leseKommazahl;
		  }
		  
		  public char gibOperator() {
		    return Tastatur.leseZeichen;
		  }
		  
		  public double addition(double zahl1,double zahl2) {
		    return zahl1+zahl2;
		  }

		  public double subtraktion(double zahl1, double zahl2) {
		    return zahl1-zahl2;
		  }
		  
		  public double mulitplikation(double zahl1, double zahl2) {
		    return zahl1*zahl2;
		  }
		  
		  public double division(double zahl1, double zahl2) {
		    return zahl1/zahl2;
		  }
		}

	}

}
