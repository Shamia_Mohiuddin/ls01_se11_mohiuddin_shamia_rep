import java.util.Scanner; // Import der Klasse Scanner

public class Konsoleneingabe_Scanner {




 
 public static void main(String[] args) // Hier startet das Programm
 {
 
 // Neues Scanner-Objekt myScanner wird erstellt 
 Scanner myScanner = new Scanner(System.in); 
 
 int zahl1;
 int zahl2;
 int ergebnis1,ergebnis2,ergebnis3,ergebnis4;
 
 System.out.print("Bitte geben Sie eine ganze Zahl ein: "); 
 zahl1 = myScanner.nextInt();
 // Die Variable eingabe1 speichert die erste Eingabe
 
 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
 // Die Variable zahl2 speichert die zweite Eingabe
 zahl2 = myScanner.nextInt(); 
 
 // Die Addition der Variablen zahl1 und zahl2 
 // wird der Variable ergebnis zugewiesen.
 ergebnis1 = zahl1 + zahl2;
 ergebnis2 = zahl1 - zahl2;
 ergebnis3 = zahl1 * zahl2;
 ergebnis4 = zahl1 / zahl2;

 
 //System.out.print("\n\n\nErgebnis der Addition lautet: ");
 //System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis); 
 myScanner.close();
 
 System.out.println(zahl1 + " + " + zahl2 + " = " +  ergebnis1);
 System.out.println(zahl1 + " - " + zahl2 + " = " +  ergebnis2);
 System.out.println(zahl1 + " * " + zahl2 + " = " +  ergebnis3);
 System.out.println(zahl1 + " / " + zahl2 + " = " +  ergebnis4);
 
 } 
}




