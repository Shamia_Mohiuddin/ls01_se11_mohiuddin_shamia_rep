import java.util.Scanner; // Import der KLasse Scanner


public class MethodenUebungen {
	
	public static void main (String[] args)   // Hier startet das Programm
	{
		
	
	// Neues Scanner-Objekt myScanner wird erstellt
	Scanner myScanner = new Scanner(System.in);
			
	System.out.print("Bitte geben Sie einen ganze Zahl ein: ");
	
	// Die Variable wert speichert die erste Eingabe
	double wert = myScanner.nextDouble();
	
	System.out.print(" Bitte geben Sie eine zweite ganze Zahl ein: ");
	
	// Die Variable wert2 speichert die zweite Eingabe
	double wert2 = myScanner.nextDouble();
	
	// Die Addition der Variablen wert und wert2
	// wird der Variable ergebnis zugewiesen.
	double ergebnis = wert + wert2;
	
	System.out.print("\n\n\nErgebnis der Addition lautet: ");
	System.out.print(wert + " + " + wert2 + " = " + ergebnis);
	
	
	myScanner.close();
	
	}
}	
	
	


